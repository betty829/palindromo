import unittest
from palindromo.Palindromo import Palindromos

class test_palindromo(unittest.TestCase):
    def setUp(self):
        self.numeroPalindromo=Palindromos()
        self.listaEnteros=Palindromos()
                
    def test_generarArreglo(self):
        self.numeroPalindromo.cantidad=10
        self.assertEqual(self.numeroPalindromo.generarArreglo(),[0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
    
    def test_hallarPalindromo(self):
        self.listaEnteros.lista=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
        self.assertEqual(self.listaEnteros.hallarPalindromo(),[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11])
